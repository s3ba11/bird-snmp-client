# bird-snmp-client

Simple bash scripts adding ability to monitor BIRD Internet Routing Daemon via snmp.
Software was written for Ubiquiti Edge OS but can be used on all linux based systems.

**Requirements**
* awk
* bash
* birdc
* grep
* sort
* sudo
* systemd (can be easily changed to old format by editing one line in bird2snmpd. Check the installation for other linux).

**Scripts**
* bird-snmp-client - Main script executing by SNMPD daemon.
* bird-snmp-client.conf - Configurable variables. Default settings for Edge OS routers.
* bird2snmpd - Checks snmpd.conf file and adding necessary 'pass' option if necessary. This script should be add to cron table.
* sudoers - This script adds privileges to run birdc by snmpd user.

**Instalation on EdgeOS**

1. Put project files in /config/scripts directory.
2. Set the configuration in bird-snmp-client.conf.
3. Add task-scheduler job.
```
system task-scheduler task bird2snmpd
 executable {
     path /config/scripts/bird2snmpd
 }
 interval 5m
```
4. Run once firstboot.d/sudoers script.

**Instalation on other linux system**
 
Generaly you only need one script: bird-snmp-client with configuration file: bird-snmp-client.conf
1. Set the configuration in bird-snmp-client.conf.
2. Put 'pass' option to your snmpd.conf (Usually in /etc/snmp/snmpd.conf) and reload snmpd daemon.
```
pass [YOUR OID] [PATH TO: bird-snmp-client].
```
Example: 
```
pass .1.3.6.1.4.1.179179 /config/scripts/bird-snmp-client
```
3. Make sure your snmpd user has privileges to run birdc. If not, you can add them using sudo. Check firstboot.d/sudoers as an example.
4. Reload snmpd service.
